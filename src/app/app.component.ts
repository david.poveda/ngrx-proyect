import { INCREMENT } from './../redux/counter/counter.actions';
import { Component } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { AppState } from './../redux/app.state';

@Component({
  selector: 'app-root',
  template: `
  <!--The content below is only a placeholder and can be replaced.-->
<div style="text-align:center">
  <h1>
    Welcome to {{ title }}!
    </h1>
    <button (click)="increment()">incrementar</button>
    <h2><a>{{ counter }}</a></h2>
</div>


<router-outlet></router-outlet>
<app-counter></app-counter>
`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngrx';
  counter: number;

  constructor(
    private store: Store<AppState>
  ) {
  this.store.select('counter').subscribe(
    (counterState) => {
      this.counter = counterState;
      console.log('initState ', counterState);
      }
    );

  }
  increment() {
  const action: Action = {
      type: INCREMENT
    };
  this.store.dispatch(action);
  }
}
