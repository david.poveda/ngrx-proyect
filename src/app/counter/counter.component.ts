import { Component, OnInit } from '@angular/core';
import { Store, Action } from '@ngrx/store';
import { DECREMENT, PLUS, PlusAction } from './../../redux/counter/counter.actions';
import { AppState } from './../../redux/app.state';
@Component({
  selector: 'app-counter',
  template: `

  <div style="text-align:center">
  <h1> Component</h1>
  <button (click)="plus()">Pus</button>
  </div>
  <div style="text-align:center">
    <button (click)="decrement()">Restar</button>
</div>

  `,
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent {

    counter: number;

    constructor(
      private store: Store<AppState>
    ) {
    this.store.select('counter').subscribe(
      (counterState) => {
        this.counter = counterState;
        console.log('initState ', counterState);
        }
      );

    }
    decrement() {
    const action: Action = {
        type: DECREMENT
      };
    this.store.dispatch(action);
    }

    plus() {
      const action: PlusAction = {
          type: PLUS,
          payload: 12
        };
      this.store.dispatch(action);
      }
  }