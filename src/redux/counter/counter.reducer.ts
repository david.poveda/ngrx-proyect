import { Action } from '@ngrx/store';
import { PlusAction, DECREMENT, INCREMENT, PLUS } from './counter.actions';


export function counterReducer(state: number = 102, action: Action) {
    if (action === null) {
        return state;
    }
    switch (action.type) {
      case INCREMENT:
        return state + 1;
      case DECREMENT:
        return state - 1;
      case PLUS:
        return state + ( action as PlusAction).payload;
      default:
        return state;
    }
  }
