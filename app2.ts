interface Action {
    type: string;
    payload?: any;
}

interface Reducer<T> {
    (oldState: T, action: Action): T
}

let countReducer: Reducer<number> = (state: number = 0, action: Action) =>{
    if(!action) {
        return state;
    }
    switch(action.type){
        case 'INCREMENT':
          return state + 1;
        case 'DECREMENT':
          return state - 1;
        case 'PLUS':
          return state + action.payload;
        default:
          return state;
      }
}

const actionIncrement: Action = {
    type: 'INCREMENT'
}

const actionDecrement: Action = {
    type: 'DECREMENT'
}

const actionPLus: Action = {
    type: 'PLUS',
    payload: 12
}
  
console.log(countReducer(34, null));//34
console.log(countReducer(12, actionIncrement));//12
console.log(countReducer(100, actionDecrement));//100
console.log(countReducer(123, actionPLus));//100
